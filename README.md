`Euromillions` Test project was bootstrapped with [Create React App]

I have chosen not to use `Webpack`. IE compatibility is supported with the help of `react-app-polyfill`. For the list of other supported browsers, please visit this link: `https://browserl.ist/?q=%3E+0.2%25%2C+not+dead%2C+not+op_mini+all`
__
If you really want to use Webpack manually, for fined grained control and dealing with exceptional cases, you should eject the project with `yarn eject`
The command will generate the webpack, babel and eslint files automatically.

## Design Pattern

I have chosen to use atomic design to better organize the project components. This improves component usability and concentrate all the logic inside `Pages` components.

I have also chosen to use React Hooks as much as possible as it makes logic reusable more easily than it would be with HOC pattern.

## How to use the app

After cloning the project run:

### `yarn install`

Then, to start the project you can run:

### `yarn start`

which runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />

If you want to launch the test runner in the interactive watch mode.<br />
### `yarn test`

See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

If you want to build a production app, run:

### `yarn build`

Which builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
