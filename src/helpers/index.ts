import _ from 'lodash';

interface Prices {
    currency: string;
    value: number;
}

export const getPriceFromPattern = (pattern: number[], pricesData: any): Prices => {
    const { multiples } = pricesData.data;
    let res = {
        currency: 'EUR',
        value: 0,
    };
    _.forEach(multiples, item => {
        if(item.pattern[0] === pattern[0] && item.pattern[1] === pattern[1] ) res = item.cost;
    });
    return res;
};
