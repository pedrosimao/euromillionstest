import React from 'react';
import styled from 'styled-components';

interface Props {
    children: any;
}

const Card = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    max-width: 60%;
    margin: auto;
`;

export const GameCard: React.FC<Props> = props => {
    return (
        <Card>
            { props.children }
        </Card>
    );
};
