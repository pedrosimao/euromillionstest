import React from 'react';
import styled from 'styled-components';

const AppHeader = styled.header`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: calc(10px + 2vmin);
    color: white;
`;

const AppLogo = styled.img`
    height: 170px;
`;

export const Header: React.FC = () => {
    return (
        <AppHeader>
            <AppLogo src='euroMillions-logo.png' className="App-logo" alt="logo" />
        </AppHeader>
    );
};
