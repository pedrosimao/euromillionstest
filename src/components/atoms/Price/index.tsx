import React from 'react';
import styled from 'styled-components';

interface Props {
    value: number;
    currency: string;
}

interface TextProps {
    weight?: number;
}

const PriceContainer = styled.div`
    width: 12em;
    height: 2.5em;
    border-style: solid;
    border-color: black;
    margin: 25px 26% 25px auto;
    padding: 10px;
    text-align: center;
`;

const Text = styled.span<TextProps>`
    color: 'black';
    font-size: 1em;
    font-weight: ${ props => props.weight || '700'} ;
`;

export const Price: React.FC<Props> = ({ value, currency }) => {
    return (
        <PriceContainer>
            <Text weight={400}>Mise totale:</Text>
            {
                value === 0
                    ? <div><Text>Jeu invalide</Text></div>
                    : (
                        <div>
                        <Text>{value / 100}</Text>
                        <Text> {currency}</Text>
                        </div>
                    )
            }
        </PriceContainer>
    );
};
