import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCircle } from '@fortawesome/free-solid-svg-icons';
import { faCircle as farCircle } from '@fortawesome/free-regular-svg-icons';

interface Props {
    selected: boolean;
    position: number;
    handleClick?: any;
    style?: any;
}

const CircleContainer = styled.div`
    width: 3.2em;
    height: 3.2em;
    position: relative;
    cursor: pointer;
`;
const PositionText = styled.span<Props>`
    display: inline-block;
    position: absolute;
    top: 0.6em;
    left: ${props => props.position > 9 ? '0.6em' : '0.9em'};
    color: ${props => props.selected ? 'white' : '#005aaa'};
    font-size: 1.2em;
    font-weight: 900;
    display: block;
    user-select: none;
`;

export const Circle: React.FC<Props> = props => {
    return (
        <CircleContainer onClick={props.handleClick}>
            <PositionText selected={props.selected} position={props.position}>
                {props.position}
            </PositionText>
            <FontAwesomeIcon
                icon={props.selected ? faCircle : farCircle}
                size={'3x'}
                color={'#005aaa'}
                style={{...props.style}}
            />
        </CircleContainer>
    );
};
