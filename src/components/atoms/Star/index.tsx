import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons';

interface Props {
    selected: boolean;
    position: number;
    handleClick?: () => void;
    style?: any;
}

const StarContainer = styled.div`
    width: 4em;
    height: 4em;
    position: relative;
    cursor: pointer;
`;

const PositionText = styled.span<Props>`
    display: inline-block;
    position: absolute;
    top: 0.75em;
    left: ${props => props.position > 9 ? '0.85em' : '1.1em'};
    color: ${props => props.selected ? 'white' : 'gold'};
    font-size: 1.2em;
    font-weight: 900;
    display: block;
    user-select: none;
`;

export const Star: React.FC<Props> = props => {
    return (
        <StarContainer onClick={props.handleClick}>
            <PositionText selected={props.selected} position={props.position}>
                {props.position}
            </PositionText>
            <FontAwesomeIcon
                icon={props.selected ? faStar : farStar}
                size={'3x'}
                color={'gold'}
                style={{...props.style}}
            />
        </StarContainer>
    );
};
