import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import _ from 'lodash';

import { getPricesData } from '../../../redux/actions/prices';
import { clickStar, clickCircle } from '../../../redux/actions/game';
import { getPriceFromPattern } from '../../../helpers';

import { Header } from '../../atoms/Header';
import { Price } from '../../atoms/Price';
import { CirclesCard } from '../../molecules/CirclesCard';
import { StarsCard } from '../../molecules/StarsCard';
import { GameCard } from '../../organisms/GameCard';

export const Game: React.FC = () => {
    // hooks
    const stars: object = useSelector((state: any) => state.game.stars);
    const circles: object = useSelector((state: any) => state.game.circles);
    const pricesData: object = useSelector((state: any) => state.prices);
    const dispatch = useDispatch();

    useEffect(() => {
            dispatch(getPricesData());
        }, [dispatch]
    );

    // variables
    const countStars: number = _.countBy(stars, (star) => star === true).true || 0;
    const countCircles: number = _.countBy(circles, (circle) => circle === true).true || 0;
    const pattern: number[] = [countCircles, countStars];
    const cost: { value: number, currency: string } = getPriceFromPattern(pattern, pricesData);

    return (
        <React.Fragment>
            <Header />
            <Price value={cost.value} currency={cost.currency}/>
            <GameCard>
                <CirclesCard
                    circles={circles}
                    handleClick={(position: number) => dispatch(clickCircle(position))}
                />
                <StarsCard
                    stars={stars}
                    handleClick={(position: number) => dispatch(clickStar(position))}
                />
            </GameCard>
        </React.Fragment>
    );
};
