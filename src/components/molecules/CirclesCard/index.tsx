import React from 'react';
import styled from 'styled-components';

import { Circle } from '../../atoms/Circle';

interface Props {
    circles: any;
    handleClick: Function;
}

const Card = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    max-width: 65%;
`;

export const CirclesCard: React.FC<Props> = ({ circles, handleClick }) => {
    return (
        <Card>
            {
                Object.keys(circles).map((position) => {
                    const selected = circles[position];
                    return <Circle
                        key={`circle${position}`}
                        selected={selected}
                        position={Number(position)}
                        handleClick={() => handleClick(position)}
                        />;
                })
            }
        </Card>
    );
};
