import React from 'react';
import styled from 'styled-components';

import { Star } from '../../atoms/Star';

interface Props {
    stars: any;
    handleClick: Function;
}

const Card = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    max-width: 30%;
    min-width: 10%;
`;

export const StarsCard: React.FC<Props> = ({stars, handleClick}) => {
    return (
        <Card>
            {
                Object.keys(stars).map((position) => {
                    const selected = stars[position];
                    return <Star
                        key={`star${position}`}
                        selected={selected}
                        position={Number(position)}
                        handleClick={() => handleClick(position)}
                    />;
                })
            }
        </Card>
    );
};
