import React from 'react';
import { Provider } from 'react-redux';
import { store } from './redux/store';
import { Game } from './components/pages/Game';

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Game />
    </Provider>
  );
};

export default App;
