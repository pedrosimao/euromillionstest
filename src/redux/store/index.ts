import { createStore, applyMiddleware, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import { game } from '../reducers/game';
import { prices } from '../reducers/prices';

const rootReducer = combineReducers({
    game,
    prices,
});

export const store = createStore(
    rootReducer,
    composeWithDevTools(
        applyMiddleware(thunk),
    ),
);

export type AppStateType = ReturnType<typeof rootReducer>
