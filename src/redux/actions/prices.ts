import { Action, Dispatch } from 'redux';
import { ThunkAction } from 'redux-thunk';
import { GameState } from '../types';
import { c, mockedData } from '../constants'

export const getPricesData = (): ThunkAction<void, GameState, null, Action<string>> => {
    const request = () => { return { type: c.GET_API_REQUEST } };
    const success = (payload: object) => { return { type: c.GET_API_SUCCESS, payload } };
    const failure = (payload: object) => { return { type: c.GET_API_FAILURE, payload } };

    return async (dispatch: Dispatch<Action>) => {
        dispatch(request());
        try {
            const mockedFetch = (): Promise<object> => new Promise((resolve) => {
                setTimeout(() => resolve(mockedData), 200);
            });
            const res: object = await mockedFetch();
            dispatch(success(res));
        } catch (error) {
            dispatch(failure(error));
            console.log('[ERROR] fetching API data failed...')
        }
    };
};
