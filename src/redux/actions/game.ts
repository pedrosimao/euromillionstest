import { c } from '../constants'
import { ClickActionTypes } from '../types'

export const clickStar = (starNumber: number): ClickActionTypes => {
    return { type: c.CLICKED_STAR, payload: starNumber };
};

export const clickCircle = (circleNumber: number): ClickActionTypes => {
    return { type: c.CLICKED_CIRCLE, payload: circleNumber };
};
