import { c } from '../constants';
import { PricesState, AsyncActionType } from '../types'

const initialState: PricesState = {
    data: {},
    error: '',
    loading: false
};

export const prices = ( state = initialState, action: AsyncActionType): PricesState => {
    switch (action.type) {
        case c.GET_API_REQUEST:
            state = {
                ...state,
                loading: true
            };
            break;
        case c.GET_API_SUCCESS:
            state = {
                ...state,
                data: action.payload,
                loading: false
            };
            break;
        case c.GET_API_FAILURE:
            state = {
                ...state,
                error: action.payload,
                loading: false
            };
            break;
        default:
            return state;
    }
    return state;
};
