import _ from 'lodash';
import { c } from '../constants';
import { ClickActionTypes, GameState } from '../types';

const circles: any = {};
const stars: any = {};

_.times(50, (i: number) => {
    circles[i + 1] = false;
});
_.times(12, (i: number) => {
    stars[i + 1] = false;
});

const initialState: GameState = {
    circles,
    stars,
};

export const game = ( state = initialState, action: ClickActionTypes): GameState => {
    switch (action.type) {
        case c.CLICKED_STAR:
            state = {
                ...state,
                stars: {
                    ...state.stars,
                    [action.payload]: !state.stars[action.payload],
                },
            };
            break;
        case c.CLICKED_CIRCLE:
            state = {
                ...state,
                circles: {
                    ...state.circles,
                    [action.payload]: !state.circles[action.payload],
                },
            };
            break;
        default:
            return state;
    }
    return state;
};
