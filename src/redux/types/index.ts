import { c } from '../constants';

interface ClickStarAction {
    type: typeof c.CLICKED_STAR
    payload: number
}

interface ClickCircleAction {
    type: typeof c.CLICKED_CIRCLE
    payload: number
}

export interface PricesState {
    loading: boolean
    data: object
    error: string
}

export interface GameState {
    stars: any
    circles: any
}

export type ClickActionTypes = ClickStarAction | ClickCircleAction

export interface AsyncActionType {
    type: typeof c.GET_API_REQUEST
    payload?: any
}

